<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->insert([[
                        'name' => 'DinDinn',
                        'email' =>'super_user@dindinn.com',
                        'age'   => 18,
                        'password' => bcrypt('password'),
                    ],
                    [
                        'name' => 'U1',
                        'email' =>'u1@luckyshine.xyz',
                        'age'   => 21,
                        'password' => bcrypt('luckyshine001'),
                    ],
                    [
                        'name' => 'U2',
                        'email' =>'u2@luckyshine.xyz',
                        'age'   => 51,
                        'password' => bcrypt('luckyshine002'),
                    ],
                    [
                        'name' => 'U3',
                        'email' =>'u3@luckyshine.xyz',
                        'age'   => 31,
                        'password' => bcrypt('luckyshine003'),
                    ],
                    [
                        'name' => 'U4',
                        'email' =>'u4@luckyshine.xyz',
                        'age'   => 18,
                        'password' => bcrypt('luckyshine004'),
                    ],
                    [
                        'name' => 'U5',
                        'email' =>'u5@luckyshine.xyz',
                        'age'   => 21,
                        'password' => bcrypt('luckyshine005'),
                    ],
                    [
                        'name' => 'U6',
                        'email' =>'u6@luckyshine.xyz',
                        'age'   => 35,
                        'password' => bcrypt('luckyshine006'),
                    ]
            ]);
    }
}
