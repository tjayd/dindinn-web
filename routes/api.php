<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['middleware' => 'auth:api'], function(){
    Route::group(['prefix' => 'treasures', 'as' => 'treasures.', 'namespace' => 'Api'], function() {
        Route::get('/','Geolocation@findTreasure')->name('findTreasure');
        Route::get('/prizes','Geolocation@findTreasureValue')->name('findTreasureValue');
    });
    Route::post('/order','Api\OrdersController@store')->name('order');
});
Route::post('/login','Api\Authentication@login')->name('login');


