<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Treasures;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;


class Geolocation extends Controller
{
    public function findTreasure(Request $request){
        $validatedData = Validator::make($request->all(),[
            'distance' => ['required', Rule::in([1, 10])],
            'latitude' => 'required|numeric',
            'longitude'=>'required|numeric'
        ]);

        if ($validatedData->fails()) {
            return response()->json(['title'=>$validatedData->errors()->keys()[0],
                                    'message' => $validatedData->errors()->first()
                                    ], 422);
        }
        $treasures = Treasures::findTreasures($request->latitude,$request->longitude,$request->distance);
        return response()->json(['data'=>$treasures],200);

    }

    public function findTreasureValue(Request $request){
        $validatedData = Validator::make($request->all(),[
            'distance' => ['required', Rule::in([1, 10])],
            'prize_value' =>'nullable|integer||between:10,30',
            'latitude' => 'required|numeric',
            'longitude'=>'required|numeric'
        ]);

        if ($validatedData->fails()) {
            return response()->json(['title'=>$validatedData->errors()->keys()[0],
                'message' => $validatedData->errors()->first()
            ], 422);
        }
        $treasures = Treasures::findTreasuresValue($request->latitude,
                                                    $request->longitude,
                                                    $request->distance,
                                                    $request->prize_value ?? 0 );

        return response()->json(['data'=>$treasures],200);
    }
}
