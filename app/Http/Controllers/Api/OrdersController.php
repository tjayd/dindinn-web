<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessOrder;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;
use Validator;
use Carbon\Carbon;
class OrdersController extends Controller
{
    public function store(Request $request){

        $validatedData = Validator::make($request->all(),[
            'delay' => 'required|integer',
            'vendor_id' => 'required|numeric',
            'details'=>'required'
        ]);

        if ($validatedData->fails()) {
            return response()->json(['title'=>$validatedData->errors()->keys()[0],
                'message' => $validatedData->errors()->first()
            ], 422);
        }
        $user = Auth::user();
        $order = new Orders();
        $order->user_id = $user->id;
        $order->vendor_id = $request->vendor_id;
        $order->details = $request->details;
        $order->schedule = Carbon::now()->addSeconds(120);
        if($order->save()){
            ProcessOrder::dispatch($order)->delay(Carbon::now()->addSeconds($request->delay));
            return response()->json(['data'=>$order],200);
        }else{
            return response()->json(['data'=>[],'message'=>'Something went wrong. Try again later','title'=>'OOPS!'],422);
        }
    }
}
