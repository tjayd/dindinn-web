<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserDevices;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

class Authentication extends Controller
{
    public function login(Request $request){
        $validatedData = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required',
            'token' => 'required',
            'device_id' => 'required',
            'os'    => ['required', Rule::in(["android", "ios"])]
        ]);
        if ($validatedData->fails()) {
            return response()->json(['title'=>$validatedData->errors()->keys()[0],
                'message' => $validatedData->errors()->first()
            ], 422);
        }
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user   = Auth::user();
            $userTokens = $user->tokens;
            foreach ($userTokens as $userToken) {
                $userToken->revoke();
            }
            $user->accessToken = $user->createToken($user->id)->accessToken;
            UserDevices::firstOrCreate(['device_id'=>$request->device_id],
                                                    ['user_id' => $user->id,
                                                    'token' => $request->token,
                                                    'os' => $request->token
                                                    ]);
            return response()->json($user,200);
        }else {
            return response()->json(['title'=>'INVALID CREDENTIALS','message' => 'Invalid e-mail or password. Please try again.'], 422);
        }
    }
}
