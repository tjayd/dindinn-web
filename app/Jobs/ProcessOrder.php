<?php

namespace App\Jobs;

use App\Models\Orders;
use App\User;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $order;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $vendor = User::where('email','vendor1@gmail.com')->first();
        $vendor_devices = $vendor->devices;
        $device_token = array();

        foreach ($vendor_devices as $device) {
            $device_token []= $device->token;
        }
        $push_android = new PushNotification('fcm');
        $push_android->setMessage([
            'notification' => [
                'title'=>'Order!',
                'body'=>strip_tags($this->order->details),
                'sound' => 'default'
            ],
            'data' => ['order_id'=>$this->order->id]
        ])
            ->setDevicesToken($device_token)
            ->send();
    }
}
