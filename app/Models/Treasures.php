<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;
use Malhal\Geographical\Geographical;

class Treasures extends Model
{
    use CrudTrait;
    use Geographical;
    protected static $kilometers = true;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'treasures';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','latitude','longitude'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function findTreasures(Float $latitude, Float $longitude, int $distance)
    {
        $treasures = Treasures::distance($latitude, $longitude)->orderBy('distance', 'ASC')->get();
//        $treasures->where('distance','<=',$distance)
        foreach ($treasures as $key => $treasure){
            if ($treasure->distance > $distance){
               unset($treasures[$key]);
            }
        }
        return $treasures;
    }

    public static function findTreasuresValue(Float $latitude, Float $longitude, int $distance, ?int $value)
    {
        $treasures = Treasures::distance($latitude, $longitude)->orderBy('distance', 'ASC')->get();
        $data = array();
        foreach ($treasures as $key => $treasure){
            if(isset($treasure->prize_value($value)->amt)) {
                $treasure->prize_money = $treasure->prize_value($value)->amt;
            }
            if ($treasure->distance <= $distance && isset($treasure->prize_value($value)->amt)){
                $data []= $treasure;
            }
        }
        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function money_value(){
        return $this->hasMany(MoneyValues::class,'treasure_id');
    }
    public function prize_value(int $prize_value){
        return $this->hasMany(MoneyValues::class,'treasure_id')
                    ->where('amt','>',$prize_value)
                    ->first();
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
