<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserDevices extends Model
{
    protected $table = 'user_devices';
    protected $guarded = ['id'];
    protected $fillable = ['user_id','token','os','device_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
