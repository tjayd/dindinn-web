<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAXW6Zfng:APA91bFRYUhKxQ0zx7xiamTWOV3OHSD7pkk8LhSEX4GcTMm3x8JuUdsNHZPcpV_AUDWq1XHicz2__cm-PLsVvV2DSP8SgDphJ6v5QzxgFNsLFwCDi8Bk8Ho55n1jcJcKIs2doB2nScOP',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase' => 'secret', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true,
    ],
];
